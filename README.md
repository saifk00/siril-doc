# Siril-doc


This repo hosts the documentation for [Siril](https://siril.org/)  
It is written with [Sphinx](https://www.sphinx-doc.org/en/master/) in RestucturedText.


## Install necessary tools (for building locally)

```bash
pip install -r requirements.txt
```

You will also need [pypandoc](https://pypi.org/project/pypandoc/) to update the commands with the provided parser.  
Depending on your OS, you may also need to install [pandoc](https://pypi.org/project/pypandoc/#Installing-pandoc).

## Useful links

[Sphinx directives](https://www.sphinx-doc.org/en/master/usage/restructuredtext/directives.html)  
[Sphinx & RST cheatsheet](https://sphinx-tutorial.readthedocs.io/cheatsheet/)

## Build (HTML)

```bash
sphinx-build doc _build
```
The built site will be stored in `_build` directory.  
Add the option -a to rebuild all the files

## Build (PDF)

```bash
sphinx-build -M latexpdf doc _build
```

The pdf will be stored at `_build/latex/siril.pdf`

## Debugging

### Cross-references

All the headers can be referenced via a `:ref:` directive thanks to Sphinx extension autosectionlabel. In order to avoid duplicates, the option `autosectionlabel_prefix_document` has been enabled to make all tags unique by prepending the path from doc root (see [help](https://www.sphinx-doc.org/en/master/usage/extensions/autosectionlabel.html#confval-autosectionlabel_prefix_document))

If you need to figure out the correct reference to a given section or subsection, you can do the following:
- cd to `_build` folder (once built...)
- open a terminal and type ```python -m sphinx.ext.intersphinx objects.inv```  

This will display all the available tags for the documentation based on autosectionlabelling.

Example:  
To reference the annotations section, you can use:
```:ref:`the annotations documentation <astrometry/annotations:annotations>` ```  

### Siril command lines referencing

To reference Siril commands in pages, please use the following syntax for a command `cmd`:

```
.. admonition:: Siril command line
   :class: sirilcommand
   
   .. include:: ./commands/cmd_use.rst

   .. include:: ./commands/cmd.rst
```

Some commands may have additional content, to be written and stored in `./commands/cmd_add.rst`, see for instance stack_add.rst.  
The commands parser will detect that such file exists and include it in Commands.rst everytime we update.  
You can of course include this file in the admo block shown above by adding :

```

   .. include:: ./commands/cmd_add.rst
```

To parse/update the commands from local root:

``` bash
python3 tools/parse_siril_commands.py
```

### Documenting a new feature

When developing a new feature in Siril, you are kindly asked to document it as well.  

The process is as follows:
1. you have the permissions to open a branch in Siril repo and in Siril-doc repo:  
  Open a branch in siril-doc with the **same name** as the feature branch.  
  Open the MR
2. you don't have permisions in Siril repo:  
  Fork siril-doc repo **with the same Gitlab account** and create a branch that has the **same name** as your feature branch.  
  Create a MR from your fork targetting `main`

You should be able to parse the commands, if required, from your branch.  

When your MR for Siril is accepted, we can then proceed to merge the doc MR.

### Useful roles

You can use the following Sphinx built-in roles for:
- key bindings : Use `:kbd:`
- menu items: Use `:guilabel:`
- menuselection: Use `:menuselection:`. The different items should be separated by `-->`


### Automatic build

If you wish to build the pages automatically while typing them, you can use [sphinx-autobuild](https://pypi.org/project/sphinx-autobuild/)  

```bash
sphinx-autobuild doc _build
```

You can then open a browser and go to http://127.0.0.1:8000/  
Everytime you save a page, it will be rebuilt (you may need to refresh with F5 to effectively see the changes).  
If you want to rebuild the full doc (not just the updated doc), you can pass the option -a as for sphinx-build (it will take longer as everything is rebuilt from scratch).

## Launch

Open the file `_build/index.html` in your browser by:  

Linux users type:
```console
$ xdg-open _build/index.html
```
macOS users type:
```console
$ open _build/index.html
```
Windows users type:
```console
$ start _build/index.html
```
