Color Management
================

Siril now offers a fully color managed workflow using ICC profiles. By ensuring
accurate and consistent color reproduction across different devices, Siril
enables you to edit your images with confidence, knowing that the colors you see
on your screen will translate faithfully to prints or other displays. You can
edit your images in color spaces better matched to the capabilities of a wide
gamut professional photo printer, thus allowing production of prints with richer
colors than are possible when working in the standard sRGB color space, and you
can produce images that your audience will be able to view consistently with
how you see them during editing, regardless of their display device.

There are several pages in this section of the documentation: between them they
aim to provide a basic outline of what color management does, how it can help
you and how it works in Siril.

.. toctree::
   :hidden:

   color-management/getting_started
   color-management/user-interface
   color-management/theory
   color-management/workflow
   color-management/profiles
   color-management/soft-proofing
   color-management/changes

