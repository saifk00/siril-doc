Background Extraction
=====================

The sky background often has an unwanted gradient caused by light pollution, 
the moon, or simply the orientation of the camera relative to the ground. This 
function samples the background at many places of the image and looks for a 
trend in the variations and removes it following a smoothed function to avoid 
removing nebulae with it.

.. figure:: ../_images/processing/bkg_extract.png
   :alt: dialog
   :class: with-shadow

   Background extraction dialog box. On the left is the polynomial version, on 
   the right RBF.

Samples can be automatically placed by providing a density (:guilabel:`Samples per 
line`) and clicking on :guilabel:`Generate`. If areas of the image are brighter than
the median by some factor :guilabel:`Grid tolerance` times sigma, then no sample 
will be placed there.

.. tip:: If you have very strong gradients, for example when imaging in high
   Bortle urban skies, even the maximum grid tolerance may be insufficient. In this
   case you can check the :guilabel:`Keep all samples` checkbox and the full sample
   grid will be populated. You will then need to remove samples from actual astronomical
   objects manually.

After generation, samples can also be added manually
(left click) or removed manually (right click).

There are two algorithms to remove the gradient:

* **RBF**: This is the most modern method. It uses the 
  `Radial basis function <https://en.wikipedia.org/wiki/Radial_basis_function>`_ 
  to synthesize a sky background in order to remove the gradient with great 
  flexibility. This flexibility can be changed with the :guilabel:`Smoothing` 
  parameter.
* **Polynomial**: The original algorithm, very practical when you want to 
  remove the gradient on the subs. Users have the choice between degree 1 
  and 4. The higher the degree, the more flexible the correction. However, a 
  too high degree can give strange results like overcorrection.
  
Some settings are avaiable:

* **Add dither**: Hit this option when color banding is produced after 
  background extraction. Dither is an intentionally applied form of noise used 
  to randomize quantization error, preventing large-scale patterns such as 
  color banding in images.
* **Correction**:

  * **Subtraction**: it is mainly used to correct additive effects, such as 
    gradients caused by light pollution or by the Moon.
  * **Division**: it is mainly used to correct multiplicative phenomena, such 
    as vignetting or differential atmospheric absorption for example. However, 
    this kind of operation should be done by master-flat correction.
    
* **Compute Background**: This will compute the synthetic background and will 
  apply the selected correction. The model is always computed from the original
  image kept in memory allowing the user to work iteratively.
* **Show original image**: Keep pressing this button to see the original image.


The background gradient of pre-processed image can be complex because the 
gradient may have rotated with the acquisition session. It can be difficult to 
completely remove it, because it’s difficult to represent it with a polynomial 
function. If this is the case, you may consider removing the gradient in the 
subexposures: in a single image, the background gradient is much simpler and 
generally follows a simple linear (degree 1) function.

.. admonition:: Siril command line
   :class: sirilcommand
  
   .. include:: ../commands/subsky_use.rst

   .. include:: ../commands/subsky.rst
   

.. admonition:: Siril command line
   :class: sirilcommand
   
   .. include:: ../commands/seqsubsky_use.rst

   .. include:: ../commands/seqsubsky.rst


.. seealso::
   For more explanations, see the corresponding tutorial
   `here <https://siril.org/tutorials/gradient/>`_.
