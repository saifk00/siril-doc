Rotational Gradient (Larson Sekanina Filter)
############################################

The rotational gradient, also called `Larson Sekanina filter <http://users.libero.it/mnico/comets/ls.htm>`_, 
is a filter that allows to remove circular structures from an image, to better 
highlight other details. This technique is particularly effective to show the 
jets coming out of the nucleus of a comet.

The principle is quite simple: this image processing consists in subtracting 
two copies of the image from each other, one of the two copies having been 
previously rotated with respect to a point defined in the image.

* If there are circular structures around this point they are not modified by 
  rotation and will disappear after rotation.
* If there are non-circular structures, like jets in the coma, they will be 
  shifted in relation to each other between the two copies and the subtraction 
  will amplify the contrast of this structure in the result.
* If the comet moves in the image, it is possible to add a radial shift.

.. figure:: ../_images/processing/rgradient_dialog.png
   :alt: dialog
   :class: with-shadow

   Dialog box of the Rotational Gradient filter.
   
In the example below, concerning the comet 46-P Wirtanen, the alignment was 
made on the comet and the stars show important trails. The comet is very 
circular and it is difficult to see details about its activity. Therefore, it
is not necessary to define a radial shift. For the rotation, an angle of
a little more than 28° was chosen (this choice was made after several attempts 
and using the undo button to go back). To choose the coordinates of the center 
of rotation, just make a selection around the cometary nucleus and click on 
:guilabel:`Use current selection`. This action will copy the coordinates of the 
centroid to the desired location.

.. figure:: ../_images/processing/rgradient_or.png
   :alt: dialog
   :class: with-shadow
   :width: 100%

   Image of a comet whose tail is barely visible.
   
A simple click on :guilabel:`Apply` will apply the filter. In our example, the tail 
becomes visible.
   
.. figure:: ../_images/processing/rgradient_fin.png
   :alt: dialog
   :class: with-shadow
   :width: 100%

   After applying the filter, the tail of the comet appears very clearly.

.. admonition:: Siril command line
   :class: sirilcommand
   
   .. include:: ../commands/rgradient_use.rst

   .. include:: ../commands/rgradient.rst

