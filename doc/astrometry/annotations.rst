###########
Annotations
###########

Annotations are glyphs displayed on top of images to depict the presence
of known sky objects, like galaxies, bright stars and so on. They come
from catalogues but can only be displayed on images for which we know
which part of the sky they represent, images that have been **plate
solved** and contain the world coordinate system (WCS) information in
their header, so only :ref:`FITS <file-formats/FITS:FITS>` or 
:ref:`Astro-TIFF <file-formats/Astro-TIFF:Astro-TIFF>` files.

.. figure:: ../_images/annotations/example.png
   :alt: example
   :class: with-shadow
   :width: 100%

   View of full annotated image

Plate solving, can be done within Siril in the 
:menuselection:`Image Information --> Image Plate Solver...` entry, or using 
external tools like
`astrometry.net <https://nova.astrometry.net/>`__ or
`ASTAP <https://www.hnsky.org/astap.htm>`__.

.. figure:: ../_images/annotations/plate_solved_gui.png
   :alt: plate solved GUI
   :class: with-shadow
   :width: 100%

   Buttons for annotations

When a plate solved image is loaded in Siril, you can see the sky
coordinates for the pixel under the mouse pointer displayed at the
bottom right corner and the buttons related to annotations become
available. The first button toggles on or off object annotations,
the second the celestial grid and the compass.

Offline annotation catalogues
*****************************

Siril comes with a predefined list of catalogues for annotations: 

* Messier catalogue (M) 
* New General Catalogue (NGC) 
* Index Catalogue (IC) 
* Lynds Catalogue of Dark Nebulae (LdN) 
* Sharpless Catalogue (Sh2)
* Star Catalogue (3661 of the brightest stars)

In addition, 2 *user defined catalogues* can be used: 

* User DeepSky Objects Catalogue  (DSO)
* User Solar System Objects Catalogue (SSO)

They are populated with the commands described in the section about
:ref:`searching for a known object <astrometry/annotations:search for a known object>`.

They are stored in the user settings directory. Their location depends on the 
operating system: 

* for *Unix-based* OS they will be in ``~/.config/siril/catalogue`` 
* on *Windows* they are in ``%LOCALAPPDATA%\siril\catalogue``.

All of the above catalogues can be enabled/disabled for display in the 
:menuselection:`Preferences menu --> Astrometry` :ref:`tab <preferences/preferences_gui:astrometry>`.

The two *user defined catalogues* can also be purged (`i.e.` deleted) via
the appropriate buttons.

A slider on the right side, allows you to easily navigate across the
catalogue list.

.. figure:: ../_images/annotations/cat-manage_view.png
   :alt: Catalogue management
   :class: with-shadow
   :width: 100%

   Catalogue management in Preferences/Astrometry

These annotation catalogues are used primarily for display purposes. Starting 
from Siril 1.3, they are also used to locate the center of the image for
astrometry or photometry tools. If the object is found locally, the resolver will
be shown as Local. If not, it will fall back using an online resolver.

.. figure:: ../_images/annotations/local_resolver.png
   :alt: Local resolver
   :class: with-shadow

   Object resolved from local annotations catalogues


Online annotation catalogues
****************************

You may want to query other databases than the ones already shipped with Siril, 
described in the 
:ref:`offline annotation catalogues <astrometry/annotations:offline annotation catalogues>`
section. This works, again, for plated-solved images only.

Starting from Siril 1.3, this is possible with the command ``conesearch``.
This new command replaces and expands capabilities previously provided by ``nomad`` 
and ``solsys`` from Siril 1.2. 

.. admonition:: Siril command line
   :class: sirilcommand
   
   .. include:: ../commands/conesearch_use.rst

   .. include:: ../commands/conesearch.rst


.. TODO::
   To be expanded when we have the new UI elements

The table below lists all the catalogues that are available, alongside 
links to the original data.

.. csv-table:: Table of available catalogues
   :file: cats.txt
   :delim: 0x09
   :widths: 30 20 50
   :header-rows: 1

The following queries are brought to you thanks to:

- Vizier (through CDS `Vizier TAP query service <https://tapvizier.cds.unistra.fr/adql/>`_)
- Simbad (through CDS `Simbad TAP query service <https://simbad.cds.unistra.fr/simbad/sim-tap/>`_)
- IMCCE (through its `skybot conesearch service <https://vo.imcce.fr/webservices/skybot/?conesearch>`_)
- NASA exoplanet archive (though its `TAP query service <https://exoplanetarchive.ipac.caltech.edu/docs/TAP/usingTAP.html>`_)

For solar system object queries, you may pass an additional parameter `-obscode=`,
the 3-symbol code for an `IAU observatory <https://vo.imcce.fr/webservices/data/displayIAUObsCodes.php>`_ 
close by to your observing location. This will improve annotations accuracy. Please note
that results may still slightly differ from those obtained by making a :ref:`direct 
ephemerids query for a specific object <astrometry/annotations:solar-system objects>`,
which uses the exact observing location (if present in the FITS header).

These additional annotations will be displayed in *RED*, to differentiate them
from offline annotations, shown in *GREEN*. These annotations will be erased as 
soon as the ``Show Objects names`` button is toggled.

.. figure:: ../_images/annotations/solsys.png
   :alt: Solar Sytem Result
   :class: with-shadow
   :width: 100%

   Result of a Search Solar System process


Extra catalogues
****************

You may want to display your own user catalogues. This can be done with the
command ``show``. This command can also be used to display, for instance, 
csv files created with the feature to find 
:ref:`comparison stars <photometry/lightcurves:generating list of comparison stars>`.

.. admonition:: Siril command line
   :class: sirilcommand
   
   .. include:: ../commands/show_use.rst

   .. include:: ../commands/show.rst

These catalogues may be any *csv* (comma-separated) file, respecting the following rules:

- comments lines if any, should start with a `#` sign
- a line should be present at the top with the column names, comma separated
- at least `ra` and `dec` columns should be provided, in decimal degrees.
- the columns can be written in no particular order
- other columns can be passed:

   + `name` (str)
   + `diameter` (double), the object diameter in arcmin
   + `mag` (double), the object magnitude
   + `type` (str), which will be appended between `()` after the name in the Console

Other columns than those listed above may be passed but they will not be used.


**List of known user catalogues:**

Sometimes, users create their own catalogues, we can try to link them
here to help everybody.  

* Variable stars, extracted from `GCVS <http://www.sai.msu.su/gcvs/gcvs/intr.htm>`__ 5.1, discussed `here
  in French <https://www.webastro.net/forums/topic/196778-annotation-sous-siril-avec-catalogue-utilisateur/#comment-2941770>`__,
  (:download:`file link <GCVS_siril_annotations.csv>`).

.. warning::
   Contrarily to the instructions discussed in the linked topic, it is not recommended
   to replace the user-DSO catalogue with such files. The usage is discouraged as
   some of them could be particularly big and would slow down tremendously every
   annotation redraw.

Search for a known object
*************************

If you know a specific object is somewhere in the image (if not, see the 
:ref:`search for an unknown object <astrometry/annotations:search for an unknown object>`
section), it is possible to add it to annotations.

Deep-sky objects
----------------

Load a plate-solved image and type :kbd:`Ctrl+Shift+/` or 
:guilabel:`Search Object...` in the pop-up menu (right-click).

A small search dialog will appear where object names can be entered.
Pressing :kbd:`Enter` will first search for this name the existing annotation
catalogues in case it already exists under another name. If not it will send an
online request to `SIMBAD <https://simbad.cds.unistra.fr/simbad/sim-fid>`_ to get the coordinates
of an object with such a name. If found, and not already in any catalogue, the
object will be added to the *Deep Sky user catalogue*.

The items of this catalogue are displayed in *ORANGE* while the objects
from the predefined catalogues are displayed in *GREEN*.

.. figure:: ../_images/annotations/user_dso.png
   :alt: User DSO
   :class: with-shadow
   :width: 100%

   Deep sky objects from user and predefined catalogues

Examples of valid input (not case sensitive): 

* ``HD 86574`` or ``HD86574`` are both valid for this star 


Solar-system objects
--------------------

From Siril version 1.2, objects from the solar system can also be searched for,
using the `Miriade
ephemcc <https://ssp.imcce.fr/webservices/miriade/api/ephemcc/>`_
service. This is done in the same manner as for Deep Sky Objects, but
prefixing the name of the object to be searched
by some keyword representing the type of object: ``a:`` for asteroids,
``c:`` for comets, ``p:`` for planets, ``dp:`` for dwarf planets and 
``s:`` for natural satellites.
If you query an image taken from a close enough date and time (same night) than
another image already annotated with SSOs, their cached positions
will be used and corrected by each object velocity as returned by the ephemerids.
The items of this catalogue are displayed in *YELLOW*.

Examples of valid inputs (not case sensitive): 

* ``c:67p`` or ``c:C/2017 T2`` are valid forms for comets 
* ``a:1`` and ``a:ceres`` are both valid for *(1) Ceres* 
* ``a:2000 BY4`` is valid for *103516 2000 BY4* 
* ``p:4`` or ``p:mars`` are both valid for Mars
* ``dp:Pluto`` is valid for Pluto
* ``s:Moon`` or ``s:Io`` is valid for natural satellites.

.. warning::
   Images that do not have a ``DATE-OBS`` header key cannot be annotated for SSOs.
   Images that do not have observer location information (SITELAT, SITELONG and
   SITEELEV header keys) will still be annotated, but assuming a geocentric 
   observer position, i.e. as if observing from center of the Earth. Depending
   on objects distance wrt. Earth, this may result in positions being slightly 
   offset from their real positions.

Command `catsearch`
-------------------
The same feature is accessible through the command ``catsearch``:

.. admonition:: Siril command line
   :class: sirilcommand
   
   .. include:: ../commands/catsearch_use.rst

   .. include:: ../commands/catsearch.rst


Search for an unknown object
****************************

Especially useful for photometry works, it is possible to identify a
star or other objects in the image by drawing a selection around them,
right clicking to bring up the context menu, and selecting the :guilabel:`PSF`
entry.

This will open the PSF window, and if it is a star it will display
the Gaussian fit parameters, but it will also display a Web link at the
bottom left of the window. Opening it will bring you to the `SIMBAD
page <https://simbad.cds.unistra.fr/simbad/sim-fcoo>`_ for the
coordinates of the object and in many cases will give you the name of
the object.

SIMBAD does not have all known objects, but the coordinates
from the page can still be used as a starting point to look for the
object in other online catalogues, for example `Gaia DR3
(VizieR) <https://vizier.cds.unistra.fr/viz-bin/VizieR-3?-source=I/355/gaiadr3>`_.




