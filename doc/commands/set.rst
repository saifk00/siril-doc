| Updates a setting value, using its variable name, with the given value, or a set of values using an existing ini file with **-import=** option.
| See GET to get values or the list of variables
| 
| Links: :ref:`get <get>`
