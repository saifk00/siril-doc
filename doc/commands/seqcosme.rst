| Same command as COSME but for the the sequence **sequencename**. Only selected images in the sequence are processed.
| 
| The output sequence name starts with the prefix "cosme\_" unless otherwise specified with option **-prefix=**
| 
| Links: :ref:`cosme <cosme>`
