| Replaces null values by **level** values. Useful before an idiv or fdiv operation, mostly for 16-bit images
