| Saves current image into a JPG file: **filename**.jpg.
| 
| The compression quality can be adjusted using the optional **quality** value, 100 being the best and default, while a lower value increases the compression ratio
