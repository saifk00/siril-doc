| Same as CONVERT but converts only DSLR RAW files found in the current working directory
| 
| Links: :ref:`convert <convert>`
