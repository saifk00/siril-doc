| Plate solve a sequence. A new sequence will be created with the prefix "ps\_". If the input sequence is SER, providing the metadata is mandatory and the output sequence will be in the FITS cube format, as SER cannot store WCS data.
| If WCS or other image metadata are erroneous or missing, arguments must be passed:
| the approximate image center coordinates can be provided in decimal degrees or degree/hour minute second values (J2000 with colon separators), with right ascension and declination values separated by a comma or a space (not mandatory for astrometry.net). A single catalogue extract will be done for the entire sequence, if there is a lot of drift that may not succeed for all images.
| focal length and pixel size can be passed with **-focal=** (in mm) and **-pixelsize=** (in microns), overriding values from images and settings.
| 
| Unless **-noflip** is specified, if images are detected as being upside-down (negative parity), they will be flipped.
| For faster star detection in big images, downsampling the image is possible with **-downscale**.
| 
| Images can be either plate solved by Siril using a star catalogue and the global registration algorithm or by astrometry.net's local solve-field command (enabled with **-localasnet**).
| The following options apply to Siril's plate solve only.
| The limit magnitude of stars used for plate solving is automatically computed from the size of the field of view, but can be altered by passing a +offset or -offset value to **-limitmag=**, or simply an absolute positive value for the limit magnitude.
| The choice of the star catalog is automatic unless the **-catalog=** option is passed: if local catalogs are installed, they are used, otherwise the choice is based on the field of view and limit magnitude. If the option is passed, it forces the use of the remote catalog given in argument, with possible values: tycho2, nomad, gaia, ppmxl, brightstars, apass
