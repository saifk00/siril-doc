.. code-block:: text

    show [-clear] [{ -list=file.csv | [name] RA Dec }] [-nolog] [-notag]