| Analyses several stars with aperture photometry in a sequence of images and produces a light curve for one, calibrated by the others. The first coordinates, in pixels if **-at=** is used or in degrees if **-wcs=** is used, are for the star whose light will be plotted, the others for the comparison stars.
| Alternatively, a list of target and reference stars can be passed in the format of the NINA exoplanet plugin star list, with the **-ninastars=** option. Siril will verify that all reference stars can be used before actually using them. A data file is created in the current directory named light_curve.dat, Siril plots the result to a PNG image if available
| The ring radii for aperture photometry can either be configured in the settings or set to a factor of the reference image's FWHM if **-autoring** is passed.
| 
| See also SEQPSF for operations on single star
| 
| Links: :ref:`seqpsf <seqpsf>`
