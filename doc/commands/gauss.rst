| Applies to the loaded image a Gaussian blur with the given **sigma**.
| 
| See also UNSHARP, the same with a blending parameter
| 
| Links: :ref:`unsharp <unsharp>`
