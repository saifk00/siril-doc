| Resamples the loaded image, either with a factor **factor** or for the target width or height provided by either of **-width=** or **-height=**. This is generally used to resize images, a factor of 0.5 divides size by 2.
| In the graphical user interface, we can see that several interpolation algorithms are proposed.
| 
| The pixel interpolation method can be specified with the **-interp=** argument followed by one of the methods in the list **no**\ [ne], **ne**\ [arest], **cu**\ [bic], **la**\ [nczos4], **li**\ [near], **ar**\ [ea]}. If **none** is passed, the transformation is forced to shift and a pixel-wise shift is applied to each image without any interpolation.
| Clamping of the bicubic and lanczos4 interpolation methods is the default, to avoid artefacts, but can be disabled with the **-noclamp** argument
