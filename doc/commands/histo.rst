| Calculates the histogram of the **layer** of the loaded image and produces file histo_[channel name].dat in the working directory.
| layer = 0, 1 or 2 with 0=red, 1=green and 2=blue
