| Crops the sequence given in argument **sequencename**. Only selected images in the sequence are processed.
| 
| The crop selection is specified by the upper left corner position **x** and **y** and the selection **width** and **height**, like for CROP.
| The output sequence name starts with the prefix "cropped\_" unless otherwise specified with **-prefix=** option
| 
| Links: :ref:`crop <crop>`
