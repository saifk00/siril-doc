| Same command as SUBSKY but for the sequence **sequencename**.
| Dithering, required for low dynamic gradients, can be disabled with **-nodither**.
| 
| The output sequence name starts with the prefix "bkg\_" unless otherwise specified with **-prefix=** option. Only selected images in the sequence are processed
| 
| Links: :ref:`subsky <subsky>`
