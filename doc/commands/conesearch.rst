| Displays stars from the local catalog by default for the loaded plate solved image, down to the provided **limit_magnitude** (13 by default for most catalogues, except 14.5 for aavso_chart, 20 for solsys, and ommitted for pgc).
| An alternate online catalog can be specified with **-cat=**, taking values
| - for stars: tycho2, nomad, gaia, ppmxl, bsc, apass, gcvs, vsx, simbad, aavso_chart
| - for exoplanets: exo
| - for deep-sky: pgc
| - for solar system objects: solsys (closest `IAU observatory code <https://vo.imcce.fr/webservices/data/displayIAUObsCodes.php>`__ can be passed with the argument **-obscode=** for better position accuracy)
| 
| For stars catalogues containing photometric data, stars with no B-V information will be kept; they can be excluded by passing **-phot**
| 
| Some catalogs (bsc, gcvs, pgc, exo, aavso_chart and solsys) will also display, by default, names alongside markers in the display (GUI only) and list them in the log. For others with larger number of objects, namely vsx and simbad, the information can also be shown but, as it may clutter the display, it is not activated by default. This behavior can be toggled on/off with the options **-tag=on|off** to display names alongside markers and **-log=on|off** to list the objects in the console log
