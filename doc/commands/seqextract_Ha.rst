| Same command as EXTRACT_HA but for the sequence **sequencename**.
| 
| The output sequence name starts with the prefix "Ha\_" unless otherwise specified with option **-prefix=**
