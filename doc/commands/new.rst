| Creates a new image filled with zeros with a size of **width** x **height**.
| 
| The image is in 32-bit format, and it contains **nb_channel** channels, **nb_channel** being 1 or 3. It is not saved, but becomes the loaded image and it is displayed and can be saved afterwards
