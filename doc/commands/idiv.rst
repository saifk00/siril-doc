| Divides the loaded image by the image **filename**.
| Result will be in 32 bits per channel if allowed in the preferences.
| 
| See also FDIV
| 
| Links: :ref:`fdiv <fdiv>`
