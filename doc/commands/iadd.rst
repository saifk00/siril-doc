| Adds the image **filename** to the loaded image.
| Result will be in 32 bits per channel if allowed in the preferences
