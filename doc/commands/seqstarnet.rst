| This command calls `Starnet++ <https://www.starnetastro.com/>`__ to remove stars from the sequence **sequencename**. See STARNET
| 
| Links: :ref:`starnet <starnet>`
