| Divides the loaded image by the image given in argument. The resulting image is multiplied by the value of the **scalar** argument. See also IDIV
| 
| Links: :ref:`idiv <idiv>`
