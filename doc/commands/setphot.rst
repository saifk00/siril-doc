| Gets or sets photometry settings, mostly used by SEQPSF. If arguments are provided, they will update the settings. None are mandatory, any can be provided, default values are shown in the command's syntax. At the end of the command, the active configuration will be printed. Aperture is dynamic unless forced, the **aperture** value from settings is not used if dynamic, FWHM is used instead. Gain is used only if not available from the FITS header
| 
| Links: :ref:`seqpsf <seqpsf>`
