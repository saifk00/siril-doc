| This command allows easy mass selection of images in the sequence **sequencename** (from **from** to **to** included). This is a selection for later processing.
| See also UNSELECT
| 
| Links: :ref:`unselect <unselect>`
