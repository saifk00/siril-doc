| Run the Photometric Color Correction on the loaded image.
| If the image has already been plate solved, the PCC can reuse the astrometric solution, otherwise, or if WCS or other image metadata is erroneous or missing, arguments for the plate solving must be passed:
| the approximate image center coordinates can be provided in decimal degrees or degree/hour minute second values (J2000 with colon separators), with right ascension and declination values separated by a comma or a space.
| focal length and pixel size can be passed with **-focal=** (in mm) and **-pixelsize=** (in microns), overriding values from image and settings.
| you can force the plate solving to be remade using the **-platesolve** flag.
| 
| Unless **-noflip** is specified and the image is detected as being upside-down, the image will be flipped if a plate solving is run.
| For faster star detection in big images, downsampling the image is possible with **-downscale**.
| 
| The limit magnitude of stars used for plate solving and PCC is automatically computed from the size of the field of view, but can be altered by passing a +offset or -offset value to **-limitmag=**, or simply an absolute positive value for the limit magnitude.
| The star catalog used is NOMAD by default, it can be changed by providing **-catalog=apass**. If installed locally, the remote NOMAD (the complete version) can be forced by providing **-catalog=nomad**
