| Same as CONVERT but converts only FITS files found in the current working directory. This is useful to avoid conversions of JPEG results or other files that may end up in the directory
| 
| Links: :ref:`convert <convert>`
