Burger Menu
==============

First page
----------

.. image:: ../_images/GUI/main-interface_mainmenu1.png
   :alt: Main Menu Page 1

.. rubric:: Image Information

Accesses the :ref:`other page <gui/burger-menu:second page>` of this menu.

.. rubric:: Preferences

Opens the :ref:`Preferences <preferences:preferences>` menu.

.. rubric:: Siril Manual

Opens the online `documentation <https://siril.readthedocs.io>`_.

.. rubric:: Check for updates

Checks if a newer version is available.

.. rubric:: Get scripts

Opens the :ref:`page <scripts:getting more scripts>` to download more scripts than the ones which are shipped with Siril.

.. rubric:: Keyboards shortcuts

Opens a panel reminding all the available :ref:`gui/shortcuts:shortcuts`.

.. rubric:: About Siril

Opens a dialog showing te version information and Credits.

Second page
-----------

.. image:: ../_images/GUI/main-interface_mainmenu2.png
   :alt: Main Menu Page 2

.. rubric:: Image Plate-Solver

Opens the :ref:`PlateSolving <astrometry/platesolving:platesolving>` dialog.

.. rubric:: Statistics

Opens the :ref:`Statistics <statistics:statistics>` dialog.

.. rubric:: Noise Estimation

Launches a noise estimation on the loaded image. Results will be writtent in the Console.

.. rubric:: Aberration Inspector

Opens the :ref:`Aberration Inspector <image-inspection:aberration inspector>` dialog.

.. rubric:: Information

Opens the :ref:`Information <gui/information_window:image information window>` dialog.

.. rubric:: FITS Header

Displays the contents of the loaded image FITS Header.

.. image:: ../_images/GUI/FITSHeader.png
   :alt: FITS Header dialog

.. rubric:: Dynamic PSF

Opens the :ref:`Dynamic PSF <dynamic-psf:dynamic psf>` dialog.