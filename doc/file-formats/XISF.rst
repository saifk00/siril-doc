PixInsight XISF
===============

PixInsight introduced a file structure called Extensible Image Serialization 
Format or XISF. This format was written to replace the FITS format, commonly 
used in astrophotography. However, this format was essentially designed by, and 
for, the PixInsight software. As a result, the advantages it could bring for the
latter are generally nil for other software. Moreover, the FITS file format is a 
recognized and widely used format in the community. NASA itself uses it for its 
own images. It's very flexible and powerful, and contains all the features 
needed for modern astrophotographic processing.

We have therefore decided to enable Siril to read this type of file, but in 
read-only mode. Saving in XISF format is not, and will not be, implemented.
